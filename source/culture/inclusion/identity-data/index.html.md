---
layout: markdown_page
title: "Identity data"
---

#### GitLab Identity Data

Data below is as of 2018-07-31.

##### Country Specific Data

| Country Information                       | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 322   | 100%        |
| Based in the US                           | 185  |  57.45%      |
| Based in the UK                           | 20    | 6.21%     |
| Based in the Netherlands                  | 13    | 4.04%       |
| Based in Other Countries                  | 104    | 32.30%      |

##### Gender Data

| Gender (All)                              | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 322   | 100%        |
| Men                                       | 256   | 79.50%      |
| Women                                     | 66    | 20.50%      |
| Other Gender Options                      | 0     | 0%          |

| Gender in Leadership                      | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 29   | 100%        |
| Men in Leadership                         | 23    | 79.31%      |
| Women in Leadership                       | 6     | 20.69%      |
| Other Gender Options                      | 0     | 0%          |

| Gender in Development                     | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 132  | 100%        |
| Men in Development                        | 119   | 90.15%      |
| Women in Development                      | 13    | 9.85%      |
| Other Gender Options                      | 0     | 0%          |

##### Race/Ethnicity Data

| Race/Ethnicity (US Only)                  | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 185   | 100%        |
| Asian                                     | 15    | 8.29%       |
| Black or African American                 | 3     | 1.66%       |
| Hispanic or Latino                        | 12    | 6.63%       |
| Native Hawaiian or Other Pacific Islander | 1     | 0.55%       |
| Two or More Races                         | 9     | 3.87%       |
| White                                     | 94    | 51.38%      |
| Unreported                                | 51    | 27.62%      |

| Race/Ethnicity in Development   (US Only) | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 43    | 100%        |
| Asian                                     | 4     | 9.30%       |
| Black or African American                 | 1     | 2.33%       |
| Hispanic or Latino                        | 3     | 6.98%       |
  Two or More Races                         | 3     | 6.98%       |
| White                                     | 20    | 46.51%      |
| Unreported                                | 12    | 27.91%      |

| Race/Ethnicity in Leadership (US Only)    | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 24    | 100%        |
| Asian                                     | 3     | 12.50%      |
| Native Hawaiian or Other Pacific Islander | 1     | 4.17%       |
| White                                     | 11    | 45.83%      |
| Unreported                                | 9     | 37.50%      |

| Race/Ethnicity (Global)                   | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 322   | 100%        |
| Asian                                     | 27    | 8.39%       |
| Black or African American                 | 6     | 1.86%       |
| Hispanic or Latino                        | 19    | 5.90%       |
| Native Hawaiian or Other Pacific Islander | 1     | 0.31%       |
| Two or More Races                         | 8     | 3.11%       |
| White                                     | 160   | 50.93%      |
| Unreported                                | 94    | 29.50%      |

| Race/Ethnicity in Development (Global)    | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 132   | 100%        |
| Asian                                     | 9     | 6.82%       |
| Black or African American                 | 3     | 2.27%       |
| Hispanic or Latino                        | 9     | 6.82%       |
| Two or More Races                         | 4     | 3.03%       |
| White                                     | 66    | 50.00%      |
| Unreported                                | 41    | 31.06%      |

| Race/Ethnicity in Leadership (Global)     | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 29    | 100%        |
| Asian                                     | 3     | 10.34%      |
| Hispanic or Latino                        | 1     |  3.45%      |
  Native Hawaiian or Other Pacific Islander | 1     | 3.45%       |
| White                                     | 12    | 41.38%      |
| Unreported                                | 12    | 41.38%      |

##### Age Distribution

| Age Distribution (Global)                 | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 322   | 100%        |
| 18-24                                     | 18    | 5.59%       |
| 25-29                                     | 74    | 22.98%      |
| 30-34                                     | 91    | 28.26%      |
| 35-39                                     | 52    | 16.15%      |
| 40-49                                     | 55    | 17.08%      |
| 50-59                                     | 30    | 9.32%       |
| 60+                                       | 2     | 0.62%       |
| Unreported                                
