---
layout: markdown_page
title: 2FA Removal
category: Support Workflows
---

### On this page
{:.no_toc}

- TOC
{:toc}

----

## Overview

Use this workflow when a request to disable [Two-factor Authentication](http://docs.gitlab.com/ee/profile/two_factor_authentication.html) on a GitLab.com account is received. 2FA can only be removed from an account if the [workflow](https://about.gitlab.com/handbook/support/workflows/services/support_workflows/2fa_removal.html#workflow) below is successful.

### User has recovery codes
Users can try and login using their saved [two-factor recovery codes](https://docs.gitlab.com/ee/user/profile/account/two_factor_authentication.html#recovery-codes).

### User has a valid SSH key:

If a user didn't save their recovery codes, new ones can be generated with the command below via SSH if they've previously added an SSH key to their account. The new recovery codes can then be used at sign in. This option is presented to users in the Zendesk macro. If they cannot use this method then move on to the manual methods below.

```
ssh git@gitlab.com 2fa_recovery_codes
```

If a user has added an SSH key to their account but receives a `Permission denied (publickey)` error when using the command above, they may need to manually register their private SSH key using `ssh-agent` if they're using a non-default SSH key pair file path. Direct the user to [this](https://docs.gitlab.com/ee/ssh/README.html#working-with-non-default-ssh-key-pair-paths) documentation on how to resolve this.

### User can provide evidence of account ownership:
If a user has lost their account recovery codes and has no SSH key registered, proving they
own the account can be difficult. In these cases, please use the [Risk Factor Worksheet](https://docs.google.com/spreadsheets/d/1NBH1xaZQSwdQdJSbqvwm1DInHeVD8_b2L08-V1QG1Qk/edit#gid=0) (internal only).

______________

**Note: as of Aug 2018 GitLab is no longer accepting government issued ID as proof of account ownership**

______________

##### Workflow

1. Apply the **"Account::2FA Removal Verification - GitLab.com"** Macro
2. Mark the ticket as "Pending"
 
##### If the user is unable to remove 2FA using the above methods and responds with the need for further verification

1. Verify the originating email is the same as is on the account.

1. Using the [Risk Factor Worksheet](https://docs.google.com/spreadsheets/d/1NBH1xaZQSwdQdJSbqvwm1DInHeVD8_b2L08-V1QG1Qk/edit#gid=0) (internal only), determine the appropriate data classification level and verification challenges that will be used.

1. Leave an internal note on the ticket with your proposed data classification level and verification challenges.

1. Request that your selections be peer-reviewed by another member of the team. If you’re reviewing the selections of another team member, ensure that you leave an internal note on the ticket that you approved them.

1. Once approved, respond and have the user go through the selected challenges then use the worksheet to assess the risk based on their responses.

1. If the user is able to pass all challenges:
   1. As an internal note, list the data classification and risk score
   1. Disable 2FA: log into your admin account and locate the username in the users table or by going to `https://gitlab.com/admin/users/usernamegoeshere`
   1. Under the account tab, disable 2FA.
   1. Mark the ticket as "Solved" 

1. If the user is unable to pass the selected challenges:
   1. Inform them that without verification we will not be able to reset 2FA.
   1. Mark the ticket as "Solved"


##### User responds with repository verification

1. Verify the file uploaded
    + File contains the provided text string.
    + File has been uploaded to a "Personal Repository"

2. Apply an "Internal Comment" with a link to the commit (if not already included)
3. Apply the ID matches, use the **Account::2FA Removal Verification - GitLab.com - Successful** Macro

##### Failed to verify 

1. Apply the **Account::2FA Removal Verification - GitLab.com - Failed** Macro

__________________

**Macros**

* [Account::2FA Removal Verification - GitLab.com](https://gitlab.zendesk.com/agent/admin/macros/103721068)
* [Account::2FA Removal Verification - GitLab.com - Failed](https://gitlab.zendesk.com/agent/admin/macros/103790308)
* [Account::2FA Removal Verification - GitLab.com - Successful](https://gitlab.zendesk.com/agent/admin/macros/103772548)

## GitLab Team Members 2FA Removal

If the user is a GitLab employee, follow the below process:

1. Perform steps for SSH key and recovery codes, if possible.

2. Confirm authenticity of the request by contacting the employee via phone or video call.

