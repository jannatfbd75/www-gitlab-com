---
layout: markdown_page
title: "Manage Team"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### Welcome to Manage! 👋
{: #welcome}

* What does the Manage team do?

The responsibilities of this team are described by the [Manage product
category](/handbook/product/categories/#manage). Among other things, this means 
working on GitLab's functionality around user, group and project administration, 
authentication, access control, and subscriptions.

* I have a question. Who do I ask?

In GitLab issues, questions should start by @ mentioning the PM for the [Manage product
category](/handbook/product/categories/#dev). GitLabbers can also use #g_manage.

### How we work

* In accordance with our [GitLab values](https://about.gitlab.com/handbook/values/)
* Transparently: nearly everything is public, we should record/livestream meetings whenever possible
* We get a chance to work on the things we want to work on
* Everyone can contribute; no silos

### Links and Resources
{: #links}
* Our Slack channel
  * #g_manage
* Calendar
  * GitLabbers can add [this calendar](https://calendar.google.com/calendar/b/1?cid=Z2l0bGFiLmNvbV9rOWYyN2lqamExaGoxNzZvbmNuMWU4cXF2a0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t) in Google Calendar.
* Meeting agendas
  * Agendas and notes from team meetings can be found [here](https://docs.google.com/document/d/1kE8udlwjAiMjZW4p1yARUPNmBgHYReK4Ks5xOJW6Tdw/edit). For transparency across the team, we should use one agenda document.