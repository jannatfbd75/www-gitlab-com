---
layout: markdown_page
title: "Monitoring Team"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### Monitoring Team
{: #monitoring}

The monitoring team is responsible for:
* Provide the tools required to enable monitoring of GitLab.com
* Package these tools to enable all customers to manage their instances easily and completely
* Build integrated monitoring solutions for customers apps into GitLab, including: metrics, logging, and tracing

This team maps to [Monitor](/handbook/product/categories/#monitor).

#### Process for adding new metrics to GitLab

The [Monitoring](/solutions/monitor/) team is responsible for providing the underlying libraries and tools to enable GitLab team members to instrument their code. When adding new metrics, we need to consider a few facets: the impact on GitLab.com, customer deployments, and whether any default alerting rules should be provided.

Recommended process for adding new metrics:

1. Open an issue in the desired project outlining the new metrics desired
1. Label with the ~Monitoring label, and ping @gl-monitoring for initial review
1. During implementation consider:
  1. The Prometheus [naming](https://prometheus.io/docs/practices/naming/) and [instrumentation](https://prometheus.io/docs/practices/instrumentation/) guidelines
  1. Impact on cardinality and performance of Prometheus
  1. Whether any alerts should be created
1. Assign to an available Monitoring team reviewer
