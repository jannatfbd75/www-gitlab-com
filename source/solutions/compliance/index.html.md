---
layout: markdown_page
title: "Building applications that meet common regulatory compliance standards"
---
## See how GitLab controls, automation, and security can help.

This will be the overall compliance page. It will talk about how DevOps can help with compliance for regulations such as SOX, GDPR, PCI, and more.
This page will include links to individual pages that drill down on specific industry regulations. 